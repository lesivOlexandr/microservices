const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const MESSAGES_PROTO_PATH = "../protos/messages.proto";

var packageDefinition = protoLoader.loadSync(MESSAGES_PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    arrays: true
});

const MessagesService = grpc.loadPackageDefinition(packageDefinition).MessagesService;
const messagesClient = new MessagesService(
    "0.0.0.0:50051",
    grpc.credentials.createInsecure()
);

export { messagesClient };
