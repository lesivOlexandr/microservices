import { runServer } from "./api/server";
import { messagesClient } from "./grps-services";

export const runGateway = async () => {
  await runServer();
}

runGateway();
