import { FastifyInstance } from 'fastify';
import messageRoutes from './messages';

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.register(messageRoutes, { prefix: 'api' });
  next();
}