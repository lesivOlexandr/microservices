import fastify, { FastifyInstance } from "fastify";
import { messagesClient } from "../../../grps-services";
import { messageSchema } from "../../schemas/message";

export default (fastify: FastifyInstance, _: unknown, next: () => void): void => {
  fastify.post('/messages', { schema: { body: messageSchema, response: { 200: messageSchema } } }, async (req) => {
    const rpcPayload = { message: req.body };

    messagesClient.sendMessage(rpcPayload, (err, data) => {
      console.log('data successfully send to remote server');
    });

    return req.body
  });

  next();
}