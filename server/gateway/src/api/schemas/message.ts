import { Type } from '@sinclair/typebox';
import { randomUUID } from 'crypto';

export const messageSchema = Type.Object({
  id: Type.Optional(Type.String({ minLength: 1, default: randomUUID() })),
  body: Type.String({ minLength: 8, maxLength: 2**32 }),
});
