import fastify from "fastify";
import fastifySwagger from "fastify-swagger";
import routes from "./routes/routes";

export const runServer = async () => {
  const server = fastify({});

  await server.register(fastifySwagger, { 
    swagger: {
      info: {
        title: 'microservices',
        version: '1.0.0',
      },
    },
    hiddenTag: 'X-HIDDEN',
    exposeRoute: true,
    routePrefix: '/documentation'
  });

  await server.register(routes)

  server.ready(err => {
    if (err) throw err;
    server.swagger();
  });

  server.listen(5000, '0.0.0.0', (err, address) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    console.log(`Server listening at ${address}`);
  });
  return server;
}
